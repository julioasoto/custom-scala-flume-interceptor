package com.julio.flume.interceptors

import scala.collection.JavaConversions.collectionAsScalaIterable

class MyCustomInterceptor(var ctx: org.apache.flume.Context) extends org.apache.flume.interceptor.Interceptor {

	override def initialize() = {
	// No-op
	}

	override def intercept(event: org.apache.flume.Event): org.apache.flume.Event = {
		val input = new String(event getBody)
		val output = (input + (ctx getString "randomoption")).getBytes
		event setBody output
		event
	}

	override def intercept(events: java.util.List[org.apache.flume.Event]): java.util.List[org.apache.flume.Event] = {
		events foreach intercept
		events
	}

	override def close() = {
	// No-op
	}
}

object MyCustomInterceptor{

	class Builder extends org.apache.flume.interceptor.Interceptor.Builder {
		var ctx: org.apache.flume.Context = _
		
		override def configure(context: org.apache.flume.Context) = {
			this ctx = context
		}

		override def build(): org.apache.flume.interceptor.Interceptor = {
			new MyCustomInterceptor(ctx)
		}
	}
}
