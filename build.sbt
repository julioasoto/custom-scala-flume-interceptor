name := "Custom_Interceptor"

version := "0.1"

scalaVersion := "2.10.4"

//resolvers ++= Seq("Cloudera Repo" at "https://repository.cloudera.com/content/repositories/releases/")

// https://mvnrepository.com/artifact/org.apache.flume/flume-ng-core
//libraryDependencies += "org.apache.flume" % "flume-ng-core" % "1.6.0-cdh5.8.2" % "provided"

// https://mvnrepository.com/artifact/org.apache.flume/flume-ng-core
libraryDependencies += "org.apache.flume" % "flume-ng-core" % "1.6.0" % "provided"

libraryDependencies += "org.apache.flume" % "flume-ng-sdk" % "1.6.0" % "provided"

// Nombre del assembly jar que se creará
jarName in assembly := "CustomInterceptor.jar"

// Nombre de la clase principal
//mainClass in assembly := Some("es.mma.vetos.MainClass")

/* Para evitar que assembly incluya Scala en sí.
   Si nuestro proyecto utiliza Scala, debemos 
   incluir scala en sí para que la aplicación
   funcione con Java. Por eso en este proyecto
   comentamos la línea: necesitamos incluir Scala.
 */
assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = true)
